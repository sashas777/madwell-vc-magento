/*

*/


jQuery( document ).ajaxSuccess(function( event, xhr, settings ) {
    if (settings.url.indexOf('admin-ajax.php')>0 && xhr.responseText.indexOf('magento-category')>0) {
        jQuery('div.vc_wrapper-param-type-dropdown').each(function () {
            if (jQuery(this).data('param_settings') !== undefined && jQuery(this).data('param_settings').class=='magento-category') {

                jQuery(this).find('select').change(function () {
                    MagentoProductsAPICall(this.value);
                });
                jQuery(this).find('select').change();
            }
        })
    }
});

function MagentoProductsAPICall(categoryId) {
    var data = {
        'action': 'magento_category_products',
        'category_id': categoryId
    };
    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    jQuery.post(ajaxurl, data, function(response) {
        jQuery('#vc_edit-form-tab-1 select.products').html(response);
    });
}