<?php
/**
 * @author     Alex Lukyanov <alukyanau@madwell.com>
 * @copyright  2017  Alex Lukyanov
 * @license     http://opensource.org/licenses/GPL-3.0  GNU General Public License, version 3 (GPL-3.0)
 */

if ( ! class_exists( 'customMultiselect' ) ) :
class customMultiselect
{
    public function customMultiselectHtml($settings, $value){
        $elementHtml='<div class="custom_multiselect_block">'
            .'<select name="' . esc_attr( $settings['param_name'] ) .
            '" class="wpb_vc_param_value wpb-input wpb-select '. esc_attr( $settings['param_name'] ) . ' '
            .esc_attr( $settings['type'] ) . '_field" multiple>';

        foreach ($settings['options'] as $valTitle=>$val) {
            $elementHtml.='<option value="'. esc_attr( $valTitle ) . '">'. esc_attr( $val ) . '</option>';
        }
        $elementHtml.='</select></div>';
        return $elementHtml;
    }
}


endif;