<?php
/*
Plugin Name: Madwell Visual Composer Magento
Plugin URI: http://madwell.com
Description: Adds Plugins for Magento
Version: 1.0.0
Author: Alex Lukyanov
Author URI: http://madwell.com
*/


if ( ! class_exists( 'Madwell_VC_Magento' ) ) :

    class Madwell_VC_Magento {

        public function __construct() {
            add_action( 'plugins_loaded', array( $this, 'init' ) );

        }

        public function init() {
            // Before VC Init Custom Elements
            add_action( 'vc_before_init', array( $this, 'vc_before_init_actions' ) );
            add_action( 'wp_ajax_magento_category_products', 'MagentoAdminAjaxController::MagentoProductCategories' );

        }

        /**
         * Load custom VC components
         */
        function vc_before_init_actions() {

            // Link VC elements's folder
            if( function_exists('vc_set_shortcodes_templates_dir') ){
                vc_set_shortcodes_templates_dir( plugin_dir_path(__FILE__) . '/vc_templates' );
            }

            // include all custom components
            $dirs = array(
                get_template_directory().'/components/',
                plugin_dir_path(__FILE__).'components/',
                plugin_dir_path(__FILE__).'params/',
                plugin_dir_path(__FILE__).'controllers/',
                plugin_dir_path(__FILE__).'menu/'
            );
            foreach ($dirs as $dir) {
                if (file_exists($dir)) {
                    foreach (glob("{$dir}*.php") as $filename) {
                        require_once $filename;
                    }
                }
            }

        }
    }

    new Madwell_VC_Magento();

endif;

