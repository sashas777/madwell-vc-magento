<?php
/**
 * @author     Alex Lukyanov <alukyanau@madwell.com>
 * @copyright  2017  Alex Lukyanov
 * @license     http://opensource.org/licenses/GPL-3.0  GNU General Public License, version 3 (GPL-3.0)
 */

if ( ! class_exists( 'MagentoAdminAjaxController' ) ) :
class MagentoAdminAjaxController
{
    public function MagentoProductCategories() {
        global $wpdb; // this is how you get access to the database
        $connector = new MagentoConnector();

        $categoryId = intval( $_POST['category_id'] );

        $endPoint='/V1/categories/'.$categoryId.'/products';
        $connector=new MagentoConnector();
        $productsJson=$connector->makeRequest($endPoint);
        $products=json_decode($productsJson);

         $options=array();

        foreach ($products as $product) {
            $options[$product->sku]=$product->sku;
        }
        $settings=array(
            "type" => "custom_multiselect",
            "class" => "",
            "heading" => __( "Products", "my-text-domain" ),
            "param_name" => "products",
            'group' => 'Category Products',
            "options" => $options
        );

        echo customMultiselect::customMultiselectHtml($settings,'');
        wp_die(); // this is required to terminate immediately and return a proper response
    }
}
endif;