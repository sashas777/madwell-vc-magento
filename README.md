#Magento API connector for Visual Composer
Installation
==============================
~~~~
composer config repositories.vc-magento vcs git@bitbucket.org:sashas777/madwell-vc-magento.git
composer require sashas777/madwell-vc-magento 1.* --no-update
composer update
~~~~ 
 
Settings
==============================
Magento API settings. 

![Alt text](https://bytebucket.org/sashas777/madwell-vc-magento/raw/3b421c2954e00c9a3d56a1a067800991681b0ce4/wp.png)

You will need to create kyes at the Magento Admin panel System->Integrations

![Alt text](https://bytebucket.org/sashas777/madwell-vc-magento/raw/bcfd6abbce41f1c4a48852901c476e351488b329/magento.png)


Known Issues:
==============================
1. When admin click edit - existing values not filled
2. When products selected they not shown at the GUI

