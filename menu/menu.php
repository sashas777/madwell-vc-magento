<?php
/**
 * @author     Alex Lukyanov <alukyanau@madwell.com>
 * @copyright  2017  Alex Lukyanov
 * @license     http://opensource.org/licenses/GPL-3.0  GNU General Public License, version 3 (GPL-3.0)
 */

add_action( 'admin_menu', 'magento_add_admin_menu' );
add_action( 'admin_init', 'magento_settings_init' );


function magento_add_admin_menu(  ) {

    add_menu_page( 'Magento API', 'Magento API', 'manage_options', 'magento_settings', 'magento_options_page' );

}


function magento_settings_init(  ) {

    register_setting( 'pluginPage', 'magento_settings' );

    add_settings_section(
        'magento_pluginPage_section',
        __( 'API Integration Keys   ', 'wordpress' ),
        'magento_settings_section_callback',
        'pluginPage'
    );

    add_settings_field(
        'magento_url',
        __( 'Website Url (with trailing slash)', 'wordpress' ),
        'magento_url_render',
        'pluginPage',
        'magento_pluginPage_section'
    );

    
    add_settings_field(
        'magento_consumer_key',
        __( 'Consumer Key', 'wordpress' ),
        'magento_consumer_key_render',
        'pluginPage',
        'magento_pluginPage_section'
    );

    add_settings_field(
        'magento_consumer_secret',
        __( 'Consumer Secret', 'wordpress' ),
        'magento_consumer_secret_render',
        'pluginPage',
        'magento_pluginPage_section'
    );

    add_settings_field(
        'magento_access_token',
        __( 'Access Token', 'wordpress' ),
        'magento_access_token_render',
        'pluginPage',
        'magento_pluginPage_section'
    );

    add_settings_field(
        'magento_access_token_secret',
        __( 'Access Token Secret', 'wordpress' ),
        'magento_access_token_secret_render',
        'pluginPage',
        'magento_pluginPage_section'
    );



}


function magento_consumer_key_render(  ) {

    $options = get_option( 'magento_settings' );
    ?>
    <input type='text' name='magento_settings[magento_consumer_key]' value='<?php echo $options['magento_consumer_key']; ?>'>
    <?php

}


function magento_consumer_secret_render(  ) {

    $options = get_option( 'magento_settings' );
    ?>
    <input type='text' name='magento_settings[magento_consumer_secret]' value='<?php echo $options['magento_consumer_secret']; ?>'>
    <?php

}


function magento_access_token_render(  ) {

    $options = get_option( 'magento_settings' );
    ?>
    <input type='text' name='magento_settings[magento_access_token]' value='<?php echo $options['magento_access_token']; ?>'>
    <?php

}


function magento_access_token_secret_render(  ) {

    $options = get_option( 'magento_settings' );
    ?>
    <input type='text' name='magento_settings[magento_access_token_secret]' value='<?php echo $options['magento_access_token_secret']; ?>'>
    <?php

}


function magento_url_render(  ) {

    $options = get_option( 'magento_settings' );
    ?>
    <input type='text' name='magento_settings[magento_url]' value='<?php echo $options['magento_url']; ?>'>
    <?php

}


function magento_settings_section_callback(  ) {

    echo __( 'You will need to make integration at the Magento Admin System->Integrations and paste integration keys', 'wordpress' );

}


function magento_options_page(  ) {

    ?>
    <form action='options.php' method='post'>

        <h2>Magento API Settings</h2>

        <?php
        settings_fields( 'pluginPage' );
        do_settings_sections( 'pluginPage' );
        submit_button();
        ?>

    </form>
    <?php

}

?>