<?php
/**
 * @author     Alex Lukyanov <alukyanau@madwell.com>
 * @copyright  2017  Alex Lukyanov
 * @license     http://opensource.org/licenses/GPL-3.0  GNU General Public License, version 3 (GPL-3.0)
 */

// Element Class 
class mvcMagentoProducts extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        vc_add_shortcode_param( 'custom_multiselect' , 'customMultiselect::customMultiselectHtml' );
        add_action( 'init', array( $this, 'mvcMagentoProductsMapping' ) );
        add_shortcode( 'mvc_magento_products', array( $this, 'mvcMagentoProductsHtml' ) );
    }

    // Element Mapping
    public function mvcMagentoProductsMapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        $endPoint='/V1/categories';
        $connector=new MagentoConnector();
        $categoriesJson=$connector->makeRequest($endPoint);
        $categories=json_decode($categoriesJson);

        $categoriesParams=array();

        foreach ($categories->children_data as $childrenCategory) {
            $categoriesParams[$childrenCategory->name]=$childrenCategory->id;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Magento Products', 'text-domain'),
                'base' => 'mvc_magento_products',
                'description' => __('Magento Products', 'text-domain'),
                'category' => __('Madwell Elements', 'text-domain'),
                'admin_enqueue_js' => array(plugins_url('/../assets/js/magento-products.js', __FILE__)),
                'icon' => plugins_url('/../assets/img/mad_fullhero.png', __FILE__),
                'params' => array(
                    array(
                        'type'        => 'dropdown',
                        'holder' => 'h1',
                        'heading'     => __( 'Choose Category', 'madwell-vc-elements' ),
                        'param_name'  => 'category',
                        'value' => __( 'Default value', 'text-domain' ),
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        "class" => 'magento-category',
                        'group' => 'Magento Category',
                        'value' => $categoriesParams,
                    ),
                    array(
                        "type" => "custom_multiselect",
                        "class" => "",
                        "heading" => __( "Products", "my-text-domain" ),
                        "param_name" => "products",
                        'group' => 'Category Products',
                        "options" => array ()
                    )
                )
            )
        );
    }

     
    // Element HTML
    public function mvcMagentoProductsHtml( $atts, $content = null ) {

        $productSkus=explode(',',$atts['products']);
        $endPoint='/V1/products/';
        $connector=new MagentoConnector();
        $products=array();
        $options = get_option( 'magento_settings' );
        $storeUrl=$options['magento_url'];

        foreach ($productSkus as $sku) {
            $productJson=$connector->makeRequest($endPoint.$sku);
            $productInfo=json_decode($productJson);
            $products[$sku]=$productInfo;
        }

        // Start output
        $output = '';

        // Start content div
        $output .= '<div class="carousel__fullwidth wrapper">';
        foreach ($products as $sku=>$product) {
            $productImages=$product->media_gallery_entries;
            $urlKey="";
            foreach ($product->custom_attributes as $attribute){
                if ($attribute->attribute_code=='url_key') {
                    $urlKey=$attribute->value;
                }
            }
            $output .= '<div class="product-wrapper product-'.$sku.'">';
            $output .= '<img src="'.$storeUrl.'pub/media/catalog/product/'.$productImages[0]->file.'"/>';
            $output .= '<a href="'.$storeUrl.$urlKey.'.html"><h1>'.$product->name.'</h1></a>';
            $output .= 'Price: '.$product->price;
            $output .= '</div>';
        }

        // Close content div
        $output .= '</div>';

        return $output;
    }


     
} // End Element Class
 
// Element Class Init
new mvcMagentoProducts();