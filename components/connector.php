<?php
/**
 * @author     Alex Lukyanov <alukyanau@madwell.com>
 * @copyright  2017  Alex Lukyanov
 * @license     http://opensource.org/licenses/GPL-3.0  GNU General Public License, version 3 (GPL-3.0)
 */

class MagentoConnector
{

    public function makeRequest($endPoint) {

        $options = get_option( 'magento_settings' );
        $consumerKey=$options['magento_consumer_key'];
        $consumerSecret=$options['magento_consumer_secret'];
        $accessToken=$options['magento_access_token'];
        $accessTokenSecret=$options['magento_access_token_secret'];
        $storeUrl=$options['magento_url'];

        $endPoint=$storeUrl."rest".$endPoint;
        $credentials = $accessToken;
        $header[] = "Content-type: application/json";
        $header[] = "Authorization: Bearer ".$credentials;
        $header[] = "Accept: application/json";
        $isPost=0;
        $params=array();

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $endPoint);
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $header ); # custom headers, see above
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            if ($isPost==1) {
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            } elseif ($isPost==2) {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                if (strlen($params)>0) {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                }
            } else {
                curl_setopt($ch, CURLOPT_POST, 0);
            }

            $responseBody = curl_exec($ch);
            curl_close($ch);
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        return $responseBody;
    }
}